<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index(Request $request, UserRepository $repo)
    {
        $page = 2;

        $em = $this->getDoctrine()->getManager();

        // get the user repository
        $developers = $em->getRepository(\App\Entity\Link::class);

        // build the query for the doctrine paginator
        $query = $developers->createQueryBuilder('u')
            ->orderBy('u.id', 'DESC')
            ->getQuery();

        //set page size
        $pageSize = '5';

        // load doctrine Paginator
        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit

        $links = [];
        foreach ($paginator as $pageItem) {
            // do stuff with results...
            $links[] = $pageItem->getId();
        }

        dd($links);

        // return stuff..
        // return [$userList, $totalItems, $pageCount];
    }

    private function getJson(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(400, 'Invalid json');
        }

        return $data;
    }
}
