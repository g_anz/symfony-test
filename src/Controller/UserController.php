<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UpdateUserRoleType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{

    public function index()
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        if (!$users) {
            return $this->json([]);
        }

        return $this->json($this->serializeUsers($users));
    }

    public function store(Request $request, ValidatorInterface $validator)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $data = $this->getJson($request);

        $form->submit($data);

        $errors = $validator->validate($user);

        if (count($errors) === 0) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json(["errors" => [], "status" => "ok", "id" => $user->getId()], 200);

        } else {

            $_errors = [];

            foreach ($errors as $error) {

                $property = $error->getPropertyPath();
                $message = $error->getMessage();

                $_errors[$property] = $message;
            }

            return $this->json(["errors" => $_errors], 422);
        }
    }

    public function update($id, Request $request, UserRepository $userRepository, ValidatorInterface $validator)
    {
        $user = $userRepository->findOneBy(['id' => $id]);

        $form = $this->createForm(UpdateUserRoleType::class, $user);

        $data = $this->getJson($request);

        $form->submit($data);

        $errors = $validator->validate($user);

        if (count($errors) === 0) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json(["errors" => [], "status" => "ok", "roles" => $user->getRoles()], 200);

        } else {

            $_errors = [];

            foreach ($errors as $error) {

                $property = $error->getPropertyPath();
                $message = $error->getMessage();

                $_errors[$property] = $message;
            }

            return $this->json(["errors" => $_errors], 422);
        }
    }

    public function delete($id, UserRepository $userRepository)
    {
        $user = $userRepository->findOneBy(['id' => $id]);

        $entityManager = $this->getDoctrine()->getManager();

        if (!$user) {
            return $this->json(["errors" => 'No user found for id ' . $id], 422);
        }

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->json(["errors" => [], "status" => "ok"], 200);

    }

    private function serializeUsers(array $users)
    {
        $data = [];
        foreach ($users as $user) {
            array_push($data, $this->serializeUser($user));
        }
        return $data;
    }

    private function serializeUser(User $user)
    {
        return array(
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'roles' => $user->getRoles(),
        );
    }

    private function getJson(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(400, 'Invalid json');
        }

        return $data;
    }

}
