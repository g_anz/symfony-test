<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkType;
use App\Repository\LinkRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LinkController extends AbstractController
{
    /**
     * @Route("/link", name="link")
     */
    public function index()
    {

        $links = $this->getDoctrine()
            ->getRepository(Link::class)
            ->findBy(['status' => "Published"]);

        if (!$links) {
            throw $this->createNotFoundException(
                'No links found'
            );
        }

        return $this->json($this->serializeLinks($links));
    }

    public function showAllLinks(Request $request, LinkRepository $linkRepository)
    {

        $data = $this->getJson($request);

        $page = $data["page"];
        $sortFiled = $data["field"];
        $sortType = $data["sort"];

        $em = $this->getDoctrine()->getManager();

        // get the user repository
        // $developers = $em->getRepository(\App\Entity\Link::class);

        // build the query for the doctrine paginator
        $query = $linkRepository->createQueryBuilder('u')
            ->orderBy("u.{$sortFiled}", $sortType)
            ->getQuery();

        //set page size
        $pageSize = '5';

        // load doctrine Paginator
        $paginator = new Paginator($query);

        // you can get total items
        $totalItems = count($paginator);

        // get total pages
        $pagesCount = ceil($totalItems / $pageSize);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult($pageSize * ($page - 1)) // set the offset
            ->setMaxResults($pageSize); // set the limit

        $links = [];

        foreach ($paginator as $link) {
            // do stuff with results...
            $links[] = $this->serializeLink($link);
        }

        return $this->json(
            [
                "items" => $links,
                "page" => $page,
                "field" => $sortFiled,
                "sort" => $sortType,
                "totalItems" => $totalItems,
                "pages" => $pagesCount,
            ]
        );

    }

    public function store(Request $request, ValidatorInterface $validator)
    {
        $link = new Link();

        $form = $this->createForm(LinkType::class, $link);

        $data = $this->getJson($request);

        $form->submit($data);

        $errors = $validator->validate($link);

        if (count($errors) === 0) {

            // Generate short url token
            $link->setShortUrl(substr(md5(uniqid(rand(), true)), 0, 6));
            $link->setUserId($this->getUser()->getId());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->json(["errors" => [], "status" => "ok"], 200);

        } else {

            $_errors = [];

            foreach ($errors as $error) {

                $property = $error->getPropertyPath();
                $message = $error->getMessage();

                $_errors[$property] = $message;
            }

            return $this->json(["errors" => $_errors], 422);
        }
    }

    public function update(
        $id,
        Request $request,
        LinkRepository $linkRepository,
        ValidatorInterface $validator
    ) {
        $link = $linkRepository->findOneBy(['id' => $id]);

        $form = $this->createForm(LinkType::class, $link);

        $data = $this->getJson($request);

        $form->submit($data);

        $errors = $validator->validate($link);

        if (count($errors) === 0) {

            // Generate short url token
            $link->setShortUrl(substr(md5(uniqid(rand(), true)), 0, 6));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->json(["errors" => [], "status" => "ok"], 200);

        } else {

            $_errors = [];

            foreach ($errors as $error) {

                $property = $error->getPropertyPath();
                $message = $error->getMessage();

                $_errors[$property] = $message;
            }

            return $this->json(["errors" => $_errors], 422);
        }
    }

    public function delete($id, LinkRepository $linkRepository)
    {
        $link = $linkRepository->findOneBy(['id' => $id]);

        $entityManager = $this->getDoctrine()->getManager();

        if (!$link) {
            return $this->json(["errors" => 'No product found for id ' . $id], 422);
        }

        $entityManager->remove($link);
        $entityManager->flush();

        return $this->json(["errors" => [], "status" => "ok"], 200);

    }

    public function redirection(Request $request, LinkRepository $linkRepository)
    {
        $shortUrl = $request->get("shortUrl");

        $link = $linkRepository->findOneBy(['shortUrl' => $shortUrl]);

        $link->setHits($link->getHits() + 1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($link);
        $entityManager->flush();

        // redirects externally
        return $this->redirect($link->getUrl());
    }

    private function serializeLinks(array $links)
    {
        $data = [];
        foreach ($links as $link) {
            array_push($data, $this->serializeLink($link));
        }
        return $data;
    }

    private function serializeLink(Link $link)
    {
        return array(
            'id' => $link->getId(),
            'name' => $link->getName(),
            'url' => $link->getUrl(),
            'shortUrl' => $link->getShortUrl(),
            'status' => $link->getStatus(),
            'hits' => $link->getHits(),
            'user_id' => $link->getUserId(),
        );
    }

    private function getJson(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(400, 'Invalid json');
        }

        return $data;
    }
}
