<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends AbstractController
{

    public function login()
    {

        $response = ["role" => ""];

        $user = $this->getUser();

        if ($user) {

            $roles = $user->getRoles();

            if (in_array("ROLE_ADMIN", $roles)) {
                $response = ["role" => "admin"];
            } else {
                $response = ["role" => "user", "id" => $user->getId()];
            }
        }
        return $this->json($response);
    }

    public function logout()
    {
        return $this->redirectToRoute('home');
    }
}
