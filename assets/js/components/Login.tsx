import React, { useState } from "react";
import { baseUrl } from "../config";
import { useHistory } from "react-router-dom";
import Nav from "./Nav";

export default function Login({ checkLogin }) {
  const [state, setState] = useState({ login: "", password: "" });
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState("");
  const history = useHistory();

  function handleChange(e) {
    const { name, value } = e.target;
    setState((state) => ({ ...state, [name]: value }));
  }

  function isFormValid(): boolean {
    return state.login !== "" && state.password !== "";
  }

  async function login() {
    setIsLoading(true);
    setIsError(false);
    setError("");
    try {
      const response = await fetch(baseUrl + "/login", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(state), // body data type must match "Content-Type" header
      });
      const data = await response.json();

      if (data.error) {
        setError(data.error);
      } else {
        const { role, id } = data;
        checkLogin("/is-logged-in");
        if (role) {
          history.push("/", { id, role });
        }
      }
    } catch (error) {
      setIsError(true);
      console.log("error");
    }
    setIsLoading(false);
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (isFormValid()) {
      login();
    }
  }

  return (
    <div>
      <div className="container">
        <div className="row d-flex justify-content-center">
        <div className="alert alert-warning mb-4 mt-1" role="alert">
              <h5>Default credential</h5>
              <h6 className="m-0">User</h6>
              <ul className="list-unstyled">
                <p className="m-0">Login: user@mail.com</p>
                <p className="m-0">Password: user</p>
              </ul>
              <h6 className="m-0">Admin</h6>
              <ul className="list-unstyled">
                <p className="m-0">Login: admin@mail.com</p>
                <p className="m-0">Password: admin</p>
              </ul>
            </div>
        </div>
        <div className="row">
          <div className="col-4 offset-sm-4 pt-3">
          
            {error && (
              <div className="p-3 mb-2 bg-danger text-white">{error}</div>
            )}
            <form onSubmit={handleSubmit}>
              {/* - */}
              {/* Login -------------------------------- */}
              {/* - */}
              <div className="form-group">
                <label>Login</label>
                <input
                  type="text"
                  name="login"
                  className="form-control"
                  onChange={handleChange}
                  value={state.login}
                  disabled={isLoading}
                />
                {!state.login && (
                  <p className="text-danger">Field must not be empty</p>
                )}
              </div>
              {/* - */}
              {/* Password ----------------------------- */}
              {/* - */}
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  onChange={handleChange}
                  value={state.password}
                  disabled={isLoading}
                />
                {!state.password && (
                  <p className="text-danger">Field must not be empty</p>
                )}
              </div>

              <button
                type="submit"
                className="btn btn-primary"
                disabled={isLoading}
              >
                {isLoading ? (
                  <>
                    <span className="spinner-border spinner-border-sm"></span>
                    <span className="pl-2">Submitting</span>
                  </>
                ) : (
                  "Submitting"
                )}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
