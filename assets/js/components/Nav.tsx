import React from "react";
import { Link } from "react-router-dom";

export default function Nav({ user }) {
  const role = user.role;

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to={"/"}>
        Home <span className="sr-only">(current)</span>
      </Link>
      <button className="navbar-toggler" type="button">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse">
        <ul className="navbar-nav mr-auto">
          {role && (
            <li className="nav-item">
              <Link
                className="nav-link"
                to={{ pathname: "/links-page", state: { ...user } }}
              >
                Links <span className="sr-only">(current)</span>
              </Link>
            </li>
          )}
          {role === "admin" && (
            <li className="nav-item">
              <Link className="nav-link" to={"/users-page"}>
                Users
              </Link>
            </li>
          )}
        </ul>
        <a
          href="https://bitbucket.org/g_anz/symfony-test"
          className="navbar-text pr-3"
          target="_blank"
        >
          bitbucket.org/g_anz/symfony-test
        </a>
        <form className="form-inline my-2 my-lg-0">
          {role ? (
            <a href="/logout" className="btn btn-outline-success my-2 my-sm-0">
              Logout
            </a>
          ) : role === "" ? (
            <Link className="nav-link" to={"/login"}>
              <button
                className="btn btn-outline-success my-2 my-sm-0"
                type="submit"
              >
                Login
              </button>
            </Link>
          ) : (
            <div className="spinner-grow" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          )}
        </form>
      </div>
    </nav>
  );
}

Nav.defaultProps = {};
