import React, { useState } from "react";
import useFetch from "../common/useFetch";
import { baseUrl } from "../config";

export default function Home() {
  const [links, isLoading, isError] = useFetch("/links");

  return (
    <div className="container-fluid">
      <div className="d-flex align-items-center pt-3">
        <h5>Links</h5>
        {isLoading && (
          <div className="spinner-grow" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        )}
      </div>
      <ul className="list-group">
        {links &&
          links.map((link: any, i) => (
            <li key={i} className="list-group-item">
              <div className="row">
                <div className="col-2">{link.name}</div>
                <div className="col-4">
                  <span className="pl-3">
                    <a
                      className="text-break"
                      href={"https://" + link.url}
                      target="_blank"
                    >
                      {link.url}
                    </a>
                  </span>
                </div>
                <div className="col-4">
                  <span className="pl-3">
                    <a
                      className="text-break"
                      href={baseUrl + "/" + link.shortUrl}
                      target="_blank"
                    >
                      {baseUrl + "/" + link.shortUrl}
                    </a>
                  </span>
                </div>
              </div>
            </li>
          ))}
      </ul>
    </div>
  );
}
