import React, { useState, useEffect } from "react";
import useStoreLinks from "../../../common/useStoreLinks";
import { is_url } from "../../../lib/functions";

export default function LinkForm({ fetchLinks, links }) {
  const [state, setState] = useState(defaultState());

  const { page, field, sort } = links;

  const [storeResult, isStoreLoading, isStoreError, doStore] = useStoreLinks();

  function handleChange(e) {
    const { name, value } = e.target;
    setState((state) => ({ ...state, [name]: value }));
  }

  function isFormValid(): boolean {
    return state.name !== "" && state.url !== "" && is_url(state.url);
  }

  function handleSubmit(e) {
    setState((state) => ({ ...state, isSubmit: true }));
    e.preventDefault();
    if (isFormValid()) {
      doStore(state);
    }
  }

  useEffect(() => {
    let didCancel = false;
    if (!didCancel) {
      if (storeResult.status === "ok") {
        setState(defaultState());
        fetchLinks({ page, field, sort });
      }
    }

    return () => {
      didCancel = true;
    };
  }, [storeResult]);

  return (
    <div>
      {/* Title ------------------------------------------ */}
      <h6>Add links</h6>

      {/* Form ---------------------------------------------- */}
      <div className="pt-2 mb-3">
        <form onSubmit={handleSubmit} className="form-row">
          {/*  */}
          {/* Name */}
          <div className="col">
            <label>Name</label>
            <input
              type="text"
              className="form-control form-control-sm"
              name="name"
              onChange={handleChange}
              disabled={isStoreLoading}
              value={state.name}
            />

            {state.isSubmit && !state.name && (
              <p className="text-danger mb-0">Field must not be empty</p>
            )}
            {state.isSubmit && storeResult.errors.name && (
              <p className="text-danger mb-0">{storeResult.errors.name}</p>
            )}
          </div>
          {/*  */}
          {/*  Long url */}
          <div className="col">
            <label>Url</label>
            <input
              type="text"
              className="form-control form-control-sm"
              name="url"
              onChange={handleChange}
              disabled={isStoreLoading}
              value={state.url}
            />
            {state.isSubmit && !state.url && (
              <p className="text-danger mb-0">Field must not be empty</p>
            )}
            {state.isSubmit && !is_url(state.url) && (
              <p className="text-danger mb-0">Field must be a valid URL</p>
            )}
            {state.isSubmit && storeResult.errors.url && (
              <p className="text-danger mb-0">{storeResult.errors.url}</p>
            )}
          </div>
          {/*  */}
          {/* Button */}
          <div className="col-2">
            <label className="invisible">button</label>
            <button
              type="submit"
              className="btn btn-primary btn-sm form-control"
              disabled={isStoreLoading}
            >
              {isStoreLoading ? (
                <>
                  <span className="spinner-border spinner-border-sm"></span>
                  <span className="pl-2">Add link</span>
                </>
              ) : (
                "Submitting"
              )}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

function defaultState() {
  return {
    name: "",
    url: "",
    isSubmit: false,
  };
}
