import React from "react";

export default function LinkLayout({
  name,
  status,
  url,
  hide,
  errors,
  isLoading,
  handleChange,
  handleSubmit,
}) {
  return (
    <div className="p-2">
      <form onSubmit={handleSubmit}>
        {/* Name ---------------------------------------------- */}
        <div className="form-group">
          <label>Name</label>
          <input
            type="text"
            name="name"
            className="form-control form-control-sm"
            onChange={handleChange}
            value={name}
            disabled={isLoading}
          />
          {/* Errors ------------------------------- */}
          {errors.name && <p className="text-danger">{errors.name}</p>}
        </div>

        {/* Url ---------------------------------------------- */}
        <div className="form-group">
          <label>Url</label>
          <input
            name="url"
            type="text"
            className="form-control form-control-sm"
            onChange={handleChange}
            value={url}
            disabled={isLoading}
          />
          {/* Errors ------------------------------- */}
          {errors.url && <p className="text-danger">{errors.url}</p>}
        </div>

        {/* Status -------------------------------------------- */}
        <div className="form-group">
          <label>Status</label>
          <select
            name="status"
            className="form-control form-control-sm col-sm-3"
            onChange={handleChange}
            value={status}
            disabled={isLoading}
          >
            <option value="Draft">Draft</option>
            <option value="Moderation">Moderation</option>
            <option value="Published">Published</option>
          </select>
          {/* Errors ------------------------------- */}
          {errors.status && <p className="text-danger">{errors.status}</p>}
        </div>

        {/* Buttons -------------------------------------------- */}
        <div className="d-flex justify-content-end align-items-center">
          {isLoading && (
            <div
              className="spinner-border spinner-border-sm text-primary"
              role="status"
            >
              <span className="sr-only">Loading...</span>
            </div>
          )}
          <button
            type="submit"
            className="btn btn-primary btn-sm mr-1 ml-2"
            disabled={isLoading}
          >
            {isLoading ? "Updating..." : "Update"}
          </button>
          <button
            type="button"
            className="btn btn-outline-secondary btn-sm"
            onClick={() => hide()}
            disabled={isLoading}
          >
            Close
          </button>
        </div>
      </form>
    </div>
  );
}
