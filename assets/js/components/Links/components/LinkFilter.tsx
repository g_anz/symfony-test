import React from "react";

export default function LinkFilter({ fetchLinks, links }) {
  const { page, sort, field } = links;

  function doSort(_field, e) {
    e.preventDefault();
    const _sort = _field === field ? invertSort(sort) : "ASC";
    fetchLinks({ page, field: _field, sort: _sort });
  }

  return (
    <div>
      <div className="row align-items-center mb-3">
        <div className="col-2">
          <a
            href="#"
            className="stretched-link text-danger"
            onClick={(e) => doSort("name", e)}
          >
            Name {field === "name" && getSortArrows(sort)}
          </a>
        </div>
        <div className="col-4">
          <a
            href="#"
            className="stretched-link text-danger"
            onClick={(e) => doSort("url", e)}
          >
            Url {field === "url" && getSortArrows(sort)}
          </a>
        </div>
        <div className="col-2">
          <a
            href="#"
            className="stretched-link text-danger"
            onClick={(e) => doSort("hits", e)}
          >
            Hits {field === "hits" && getSortArrows(sort)}
          </a>
        </div>
      </div>
    </div>
  );
}

function invertSort(sortType: string) {
  return sortType === "DESC" ? "ASC" : "DESC";
}

function getSortArrows(sortType: string): JSX.Element {
  return sortType === "ASC" ? <span>&#8593;</span> : <span>&#8595;</span>;
}
