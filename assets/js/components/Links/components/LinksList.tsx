import React, { useState, useEffect } from "react";
import useUpdateLink from "../../../common/useUpdateLink";
import LinkItem from "./LinkItem";
import LinkLayout from "./LinkLayout";
import useDeleteLink from "../../../common/useDeleteLink";
import LinkFilter from "./LinkFilter";
import LinkPagination from "./LinkPagination";

interface Link {
  id: string;
  name: string;
  status: string;
  url: string;
  hits: number;
  shortUrl: string;
}

export default function LinksList({ links, isLoading, role, fetchLinks }) {
  const { page, pages, field, sort } = links;

  const [state, setState] = useState(defaultState());

  const [uResult, isUpdating, isUpdateEr, updateLink] = useUpdateLink();
  const [dResult, isDeleting, isDeleteEr, deleteLink] = useDeleteLink();

  function show(link) {
    setState(link);
  }

  function hide() {
    setState(defaultState());
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setState((state) => ({ ...state, [name]: value }));
  }

  function isFormValid() {
    const { id, name, url, status } = state;
    return id && name && url && status ? true : false;
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (isFormValid()) {
      updateLink(state);
    }
  }

  useEffect(() => {
    let didCancel = false;
    if (!didCancel) {
      if (uResult.status === "ok" || dResult.status === "ok") {
        setState(defaultState());
        fetchLinks({ page, field, sort });
      }
    }

    return () => {
      didCancel = true;
    };
  }, [uResult, dResult]);

  return (
    <div>
      <div className="d-flex justify-content-between mt-5">
        <div className="d-flex">
          <h6 className="mr-2">Links list</h6>
          {isLoading && (
            <div className="spinner-grow spinner-grow-sm" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          )}
        </div>

        {!isLoading && <LinkPagination fetchLinks={fetchLinks} links={links} />}
      </div>
      <ul
        className="list-unstyled mt-4"
        style={{ opacity: isLoading ? "0.5" : "1", transition: ".2s" }}
      >
        <li>
          <LinkFilter fetchLinks={fetchLinks} links={links} />
        </li>
        {links.items &&
          links.items.map(
            ({ id, name, status, url, shortUrl, hits }: Link, i) => {
              return (
                <li key={i} className="p-1 mb-2 bg-light text-dark">
                  {id === state.id ? (
                    <LinkLayout
                      name={state.name}
                      status={state.status}
                      url={state.url}
                      hide={hide}
                      handleChange={handleChange}
                      handleSubmit={handleSubmit}
                      errors={uResult.errors}
                      isLoading={isUpdating}
                    />
                  ) : (
                    <LinkItem
                      name={name}
                      status={status}
                      url={url}
                      shortUrl={shortUrl}
                      show={show}
                      id={id}
                      role={role}
                      hits={hits}
                      deleteLink={deleteLink}
                      isDeleting={isDeleting}
                    />
                  )}
                </li>
              );
            }
          )}
      </ul>
    </div>
  );
}

function defaultState() {
  return { id: "", name: "", url: "", status: "" };
}
