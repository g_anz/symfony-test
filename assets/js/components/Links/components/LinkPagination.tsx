import React from "react";

export default function LinkPagination({ fetchLinks, links }) {
  const { pages, page, field, sort } = links;

  return (
    <div>
      <nav aria-label="Page navigation example">
        <ul className="pagination pagination-sm mb-0">
          {/* Previous ---------------------------------------------------- */}
          <li className={getPageClass(page, 1, "disabled")}>
            <a
              className="page-link"
              href="#"
              onClick={(e) =>
                handleClick(e, fetchLinks, { page: page - 1, field, sort })
              }
            >
              Previous
            </a>
          </li>

          {/* First number -------------------------------------------------- */}
          {page > 2 && (
            <>
              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={(e) =>
                    handleClick(e, fetchLinks, { page: 1, field, sort })
                  }
                >
                  1
                </a>
              </li>
              {page !== 3 && (
                <li className="page-item">
                  <a className="page-link">...</a>
                </li>
              )}
            </>
          )}

          {/* Previous number ---------------------------------------------- */}
          {page > 1 && (
            <li className="page-item">
              <a
                className="page-link"
                href="#"
                onClick={(e) =>
                  handleClick(e, fetchLinks, { page: page - 1, field, sort })
                }
              >
                {page - 1}
              </a>
            </li>
          )}

          {/* Current -------------------------------------------------- */}

          <li className="page-item active">
            <a className="page-link">{page}</a>
          </li>

          {/* Next number -------------------------------------------------- */}
          {page < pages && (
            <li className="page-item">
              <a
                className="page-link"
                href="#"
                onClick={(e) =>
                  handleClick(e, fetchLinks, { page: page + 1, field, sort })
                }
              >
                {page + 1}
              </a>
            </li>
          )}

          {/* Last number -------------------------------------------------- */}
          {pages > 2 && page < pages - 1 && (
            <>
              {page !== pages - 2 && (
                <li className="page-item">
                  <a className="page-link">...</a>
                </li>
              )}

              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={(e) =>
                    handleClick(e, fetchLinks, { page: pages, field, sort })
                  }
                >
                  {pages}
                </a>
              </li>
            </>
          )}

          {/* Next ------------------------------------------------------- */}
          <li className={getPageClass(page, pages, "disabled")}>
            <a
              className="page-link"
              href="#"
              onClick={(e) =>
                handleClick(e, fetchLinks, { page: page + 1, field, sort })
              }
            >
              Next
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}

function getPageClass(page, i, className) {
  return page === i ? "page-item " + className : className;
}

function handleClick(e, fetch, data) {
  e.preventDefault();
  fetch(data);
}
