import React from "react";
import { baseUrl } from "../../../config";

export default function LinkItem({
  id,
  name,
  status,
  url,
  shortUrl,
  show,
  role,
  hits,
  deleteLink,
  isDeleting,
}) {
  return (
    <div className="row align-items-center">
      <div className="col-2">{name}</div>
      <div className="col-4">
        <ul className="list-unstyled">
          <li>
            <a className="text-break" href={"https://" + url} target="_blank">
              {url}
            </a>
          </li>
          <li>
            <a
              className="text-break"
              href={baseUrl + "/" + shortUrl}
              target="_blank"
            >
              {baseUrl + "/" + shortUrl}
            </a>
          </li>
        </ul>
      </div>
      <div className="col-1">
        <span className="pr-1">Hits:</span>{hits}
      </div>
      <div className="col-3 text-right">
        <span className="">{status}</span>
      </div>
      {role === "admin" && (
        <>
          <div className="col-2 text-right">
            <button
              type="button"
              className="btn btn-sm btn-outline-primary"
              onClick={() => show({ id, name, status, url })}
              disabled={isDeleting === id}
            >
              Edit
            </button>
            <button
              type="button"
              className="btn btn-sm btn-danger ml-1"
              onClick={() => deleteLink(id)}
              disabled={isDeleting === id}
            >
              {isDeleting === id ? (
                <>
                  <div
                    className="spinner-border spinner-border-sm"
                    role="status"
                  ></div>
                  Del...
                </>
              ) : (
                "Delete"
              )}
            </button>
          </div>
        </>
      )}
    </div>
  );
}
