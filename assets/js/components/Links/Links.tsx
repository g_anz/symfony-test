import React from "react";
import useFetch from "../../common/useFetch";
import { useLocation } from "react-router-dom";
import LinkForm from "./components/LinkForm";
import LinksList from "./components/LinksList";
import useFetchLinks from "../../common/useFetchLinks";

export default function Links() {
  const location = useLocation();
  const { id = "all", role } = location.state;

  const [links, isLoading, isError, fetchLinks] = useFetchLinks({
    page: 1,
    field: "name",
    sort: "DESC",
  });

  return (
    <div>
      <div className="container-fluid pt-5">
        {/* Link form ---------------------------------------- */}
        <LinkForm fetchLinks={fetchLinks} links={links} />
        {/* Links list --------------------------------------- */}
        <LinksList
          isLoading={isLoading}
          links={links}
          role={role}
          fetchLinks={fetchLinks}
        />
        <div style={{ height: "200px" }}></div>
      </div>
    </div>
  );
}
