import React from "react";
import useFetch from "../../common/useFetch";
import UserForm from "./components/UserForm";
import UserList from "./components/UserList";

export default function Users() {
  const [users, isLoading, isError, fetchUsers] = useFetch("/users");

  return (
    <div>
      <div className="container-fluid pt-5">
        <UserForm fetchUsers={fetchUsers} />
        <UserList users={users} isLoading={isLoading} fetchUsers={fetchUsers} />
      </div>
    </div>
  );
}
