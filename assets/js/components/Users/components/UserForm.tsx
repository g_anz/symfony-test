import React, { useState, useEffect } from "react";
import useStoreUser from "../../../common/useStoreUser";

export default function UserForm({ fetchUsers }) {
  const [state, setState] = useState({
    email: "",
    password: "",
    user: true,
    admin: false,
    isSubmit: false,
  });

  const [storeResult, isStoreLoading, isStoreError, doStore] = useStoreUser();

  function handleChange(e) {
    const { target } = e;
    const { name } = target;
    const value =
      name === "user" || name === "admin" ? target.checked : target.value;

    setState((state) => ({ ...state, [name]: value }));
  }

  function isFormValid(): boolean {
    return (
      state.email !== "" && state.password !== "" && (state.user || state.admin)
    );
  }

  function handleSubmit(e) {
    setState((state) => ({ ...state, isSubmit: true }));
    e.preventDefault();
    if (isFormValid()) {
      const roles = [];
      if (state.user) roles.push("ROLE_USER");
      if (state.admin) roles.push("ROLE_ADMIN");
      doStore({ ...state, roles });
    }
  }

  useEffect(() => {
    let didCancel = false;
    if (!didCancel) {
      if (storeResult.status === "ok") {
        fetchUsers("/users");
      }
    }

    return () => {
      didCancel = true;
    };
  }, [storeResult]);

  return (
    <div>
      <div className="d-flex align-items-center">
        {/* Title ------------------------------------------ */}
        <h6>Add User</h6>
      </div>

      {/* Form ---------------------------------------------- */}

      <div className="pt-2 mb-3">
        <form onSubmit={handleSubmit}>
          {/*  */}
          {/* Email */}
          <div className="form-group row">
            <label className="col-sm-1 col-form-label-sm">Email</label>
            <div className="col-sm-3">
              <input
                type="text"
                className="form-control form-control-sm"
                name="email"
                onChange={handleChange}
                disabled={isStoreLoading}
                value={state.email}
              />
              {state.isSubmit && !state.email && (
                <p className="text-danger mb-0">Field must not be empty</p>
              )}
              {state.isSubmit && storeResult.errors.email && (
                <p className="text-danger mb-0">{storeResult.errors.email}</p>
              )}
            </div>
          </div>
          {/*  */}
          {/*  Password */}
          <div className="form-group row">
            <label className="col-sm-1 col-form-label-sm">Password</label>
            <div className="col-sm-3">
              <input
                type="text"
                className="form-control form-control-sm"
                name="password"
                onChange={handleChange}
                disabled={isStoreLoading}
                value={state.password}
              />
              {state.isSubmit && !state.password && (
                <p className="text-danger mb-0">Field must not be empty</p>
              )}
              {state.isSubmit && storeResult.errors.password && (
                <p className="text-danger mb-0">
                  {storeResult.errors.password}
                </p>
              )}
            </div>
          </div>
          {/*  */}
          {/*  Role */}
          <div className="form-group row mb-0">
            <label className="col-sm-1 col-form-label-sm">Role</label>

            <div className="col-sm-5">
              <div className="form-check form-check-inline">
                <label className="form-check-label">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    onChange={handleChange}
                    disabled={isStoreLoading}
                    name="user"
                    checked={state.user}
                  />
                  User
                </label>
              </div>
              <div className="form-check form-check-inline">
                <label className="form-check-label">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    onChange={handleChange}
                    disabled={isStoreLoading}
                    name="admin"
                    checked={state.admin}
                  />
                  Admin
                </label>
              </div>
              {state.isSubmit && !state.user && !state.admin && (
                <p className="text-danger mb-0">
                  Both fields must not be empty
                </p>
              )}
              {state.isSubmit && storeResult.errors.role && (
                <p className="text-danger mb-0">{storeResult.errors.roles}</p>
              )}
            </div>
          </div>
          {/*  */}
          {/* Button */}
          <div className="form-group row">
            <label className="col-sm-1 col-form-label-sm"></label>
            <div className="col-sm-3">
              <button
                type="submit"
                className="btn btn-primary btn-sm"
                disabled={isStoreLoading}
              >
                {isStoreLoading ? (
                  <>
                    <span className="spinner-border spinner-border-sm"></span>
                    <span className="pl-2">Add user</span>
                  </>
                ) : (
                  "Submitting"
                )}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
