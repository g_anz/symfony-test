import React, { useState, useEffect } from "react";
import useUpdateUser from "../../../common/useUpdateUser";
import useDeleteUser from "../../../common/useDeleteUser";
import UserItem from "./UserItem";

interface User {
  id: string;
  user: boolean;
  admin: boolean;
  email: string;
  roles: [];
}

export default function UserList({ users, isLoading, fetchUsers }) {
  const [state, setState] = useState(defaultState());
  const [uResult, isUpdating, isUpdateEr, updateUser] = useUpdateUser();
  const [dResult, isDeleting, isDeleteEr, deleteUser] = useDeleteUser();

  function handleChange(e) {
    const { target } = e;
    const { name } = target;
    const value =
      name === "user" || name === "admin" ? target.checked : target.value;

    setState((state) => ({ ...state, [name]: value }));
  }

  function edit(id, roles) {
    setState({
      id: id,
      user: hasUser(roles),
      admin: hasAdmin(roles),
    });
  }

  function close() {
    setState(defaultState());
  }

  function update() {
    if (isFormValid()) return;
    const roles = [];
    const { id, admin, user } = state;

    admin && roles.push("ROLE_ADMIN");
    user && roles.push("ROLE_USER");

    updateUser({ id, roles });
  }

  function remove(id) {
    deleteUser(id);
  }

  function isFormValid() {
    return !state.user && !state.admin ? true : false;
  }

  useEffect(() => {
    let didCancel = false;
    if (!didCancel) {
      if (uResult.status === "ok" || dResult.status === "ok") {
        fetchUsers("/users");
        close();
      }
    }

    return () => {
      didCancel = true;
    };
  }, [uResult, dResult]);

  return (
    <div>
      <div className="d-flex mt-5">
        <h6 className="mr-2">Users list</h6>
        {isLoading && (
          <div className="spinner-grow spinner-grow-sm" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        )}
      </div>
      <ul
        className="list-group mt-4 list-unstyled"
        style={{ opacity: isLoading ? "0.5" : "1", transition: ".2s" }}
      >
        {users &&
          users.map(({ id, email, roles }: User, i) => (
            <li key={i}>
              <UserItem
                state={state}
                id={id}
                email={email}
                roles={roles}
                isDeleting={isDeleting}
                handleChange={handleChange}
                hasAdmin={hasAdmin}
                hasUser={hasUser}
                isFormValid={isFormValid}
                update={update}
                isUpdating={isUpdating}
                close={close}
                edit={edit}
                remove={remove}
              />
            </li>
          ))}
      </ul>
    </div>
  );
}

function defaultState() {
  return { id: "", user: false, admin: false };
}

function hasAdmin(roles: Array<string>): boolean {
  return roles.includes("ROLE_ADMIN");
}

function hasUser(roles: Array<string>): boolean {
  return roles.includes("ROLE_USER");
}
