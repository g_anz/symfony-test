import React from "react";

export default function UserItem({
  id,
  email,
  roles,
  state,
  isDeleting,
  handleChange,
  hasAdmin,
  hasUser,
  isFormValid,
  update,
  isUpdating,
  close,
  edit,
  remove,
}) {
  return (
    <div
      className="list-group-item"
      style={isDeleting && state.id === id ? { opacity: "0.3" } : {}}
    >
      <div className="row justify-content-between align-items-center">
        <div className="col-4">{email}</div>
        <div className="col-4">
          {state.id === id ? (
            <>
              <div>
                <div className="form-check form-check-inline">
                  <label className="form-check-label">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      onChange={handleChange}
                      //   disabled={isUpdating}
                      name="user"
                      checked={state.id === id ? state.user : hasUser(roles)}
                    />
                    User
                  </label>
                </div>
                <div className="form-check form-check-inline">
                  <label className="form-check-label">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      onChange={handleChange}
                      //   disabled={isUpdating}
                      name="admin"
                      checked={state.id === id ? state.admin : hasAdmin(roles)}
                    />
                    Admin
                  </label>
                </div>
              </div>
              {isFormValid() && (
                <p className="text-danger mb-0">
                  Both fields must not be unchecked
                </p>
              )}
            </>
          ) : (
            roles.map((role, i) => (
              <span className="mr-1" key={i}>
                {role}
              </span>
            ))
          )}
        </div>
        <div className="col-4 d-flex justify-content-end">
          {state.id === id &&
          (hasAdmin(roles) !== state.admin || hasUser(roles) !== state.user) ? (
            <button
              type="button"
              className="btn btn-primary btn-sm mr-2"
              onClick={() => update()}
            >
              {isUpdating ? (
                <>
                  Updating{" "}
                  <div
                    className="spinner-border spinner-border-sm"
                    role="status"
                  ></div>
                </>
              ) : (
                "Update"
              )}
            </button>
          ) : state.id === id ? (
            <button
              type="button"
              className="btn btn-primary btn-sm mr-2"
              onClick={close}
            >
              Close
            </button>
          ) : (
            <button
              type="button"
              className="btn btn-primary btn-sm"
              onClick={() => edit(id, roles)}
            >
              Edit
            </button>
          )}
          {state.id === id && (
            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={() => remove(id)}
            >
              {isDeleting ? (
                <>
                  Deleting{" "}
                  <div
                    className="spinner-border spinner-border-sm"
                    role="status"
                  ></div>
                </>
              ) : (
                "Delete"
              )}
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
