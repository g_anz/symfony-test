import "../css/app.css";
import React from "react";
import ReactDOM from "react-dom";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import "../css/app.css";
import Links from "./components/Links";
import useFetch from "./common/useFetch";
import Nav from "./components/Nav";
import Users from "./components/Users";

export default function App() {
  const [user, isLoading, isError, checkLogin] = useFetch("/is-logged-in");

  return (
    <Router>
      <Switch>
        <Route path="/login">
          <Login checkLogin={checkLogin} />
        </Route>
        <Route path="/links-page">
          <Nav user={user} />
          <Links />
        </Route>
        <Route path="/users-page">
          <Nav user={user} />
          <Users />
        </Route>
        <Route path="/">
          <Nav user={user} />
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
