export function is_url(str: string): boolean {
  const regexp: RegExp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  if (regexp.test(str)) {
    return true;
  } else {
    return false;
  }
}
