export const baseUrl: string =
  process.env.NODE_ENV === "development"
    ? "https://127.0.0.1:8000"
    : "http://1dev.online";
