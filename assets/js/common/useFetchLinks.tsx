import { useEffect, useState } from "react";
import { baseUrl } from "../config";

interface Query {
  page: number;
  field: string;
  sort: string;
}

export default function useFetchLinks(
  initialQuery: Query
): [[], boolean, boolean, React.Dispatch<Query>] {
  const [query, setQuery] = useState<Query>(initialQuery);
  const [data, setData] = useState<[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>();

  useEffect(() => {
    let didCancel = false;

    async function updateLink() {
      if (query) {
        setIsLoading(true);
        try {
          const response = await fetch(baseUrl + "/links/all", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(query),
          });

          if (response.status === 200 || response.status === 422) {
            const data = await response.json();
            setData(data);
          } else {
            setIsError(true);
          }
        } catch (error) {
          setIsError(true);
          console.log(error);
        }
        setIsLoading(false);
      }
    }

    if (!didCancel) updateLink();

    return () => {
      didCancel = true;
    };
  }, [query]);

  return [data, isLoading, isError, setQuery];
}
