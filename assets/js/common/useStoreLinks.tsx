import { useEffect, useState } from "react";
import { baseUrl } from "../config";

interface Errors {
  errors: {
    [key: string]: string;
  };
  status: string;
}

export default function useStoreLinks(): [
  Errors,
  boolean,
  boolean,
  React.Dispatch<object>
] {
  const [query, setQuery] = useState<object>();
  const [data, setData] = useState<Errors>({ errors: {}, status: "" });
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>();

  useEffect(() => {
    let didCancel = false;

    async function fetchLinks() {
      if (query) {
        setIsLoading(true);
        setData({ errors: {}, status: "" })

        try {
          const response = await fetch(baseUrl + "/links", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(query),
          });

          if (response.status === 200 || response.status === 422) {
            const data = await response.json();
            setData(data);
          } else {
            setIsError(true);
          }
        } catch (error) {
          setIsError(true);
          console.log(error);
        }
        setIsLoading(false);
      }
    }

    if (!didCancel) fetchLinks();

    return () => {
      didCancel = true;
    };
  }, [query]);
  
  return [data, isLoading, isError, setQuery];
}
