import { useEffect, useState } from "react";
import { baseUrl } from "../config";

export default function useFetch(
  initValue: string
): [Array<object>, boolean, boolean, Function] {
  const [path, setPath] = useState(initValue);
  const [data, setData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    let didCancel = false;
    async function fetchLinks() {
      if (path) {
        setIsLoading(true);
        try {
          const response = await fetch(baseUrl + path);
          if (response.status === 200) {
            const data = await response.json();
            setData(data);
          } else {
            setIsError(true);
          }
        } catch (error) {
          setIsError(true);
          console.log(error);
        }
        setIsLoading(false);
        setPath("");
      }
    }
    if (!didCancel) fetchLinks();
    return () => {
      didCancel = true;
    };
  }, [path]);
  return [data, isLoading, isError, setPath];
}
