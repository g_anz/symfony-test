import { useEffect, useState } from "react";
import { baseUrl } from "../config";

interface Errors {
  errors: {
    [key: string]: string;
  };
  status: string;
}

export default function useDeleteUser(): [
  Errors,
  boolean | string,
  boolean,
  React.Dispatch<string>
] {
  const [query, setQuery] = useState<string>();
  const [data, setData] = useState<Errors>({ errors: {}, status: "" });
  const [isLoading, setIsLoading] = useState<boolean | string>(false);
  const [isError, setIsError] = useState<boolean>();

  useEffect(() => {
    let didCancel = false;

    async function deleteUser() {
      if (query) {
        setIsLoading(query);
        setData({ errors: {}, status: "" });

        try {
          const response = await fetch(baseUrl + "/users/" + query, {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(query),
          });

          if (response.status === 200 || response.status === 422) {
            const data = await response.json();
            setData(data);
          } else {
            setIsError(true);
          }
        } catch (error) {
          setIsError(true);
          console.log(error);
        }
        setIsLoading(false);
      }
    }

    if (!didCancel) deleteUser();

    return () => {
      didCancel = true;
    };
  }, [query]);

  return [data, isLoading, isError, setQuery];
}
